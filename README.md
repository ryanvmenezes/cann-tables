# Cann Tables

An alternative way to display the traditional league table | [canntables.com](https://canntables.com)

## Let's Encrypt

`sudo letsencrypt certonly -d canntables.com --manual`
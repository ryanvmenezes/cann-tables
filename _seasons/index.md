---
layout: season
season: "2019/20"
latest: true
leagues:
- code: de
- code: en
- code: es
- code: fr
- code: it
- code: nl
- code: pt
---
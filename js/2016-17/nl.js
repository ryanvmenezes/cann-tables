var nl = {
  "competition": "Eredivisie",
  "code_string": "nl",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Eredivisie#Standings",
  "teams": 18,
  "cl": [1,2],
  "el": [3,4,5],
  "relegation": [16,18],
  "standings": [
    {
      "position": 1,
      "name": "Feyenoord",
      "played": 34,
      "points": 82,
      "difference": 61
    },
    {
      "position": 2,
      "name": "Ajax",
      "played": 34,
      "points": 81,
      "difference": 56
    },
    {
      "position": 3,
      "name": "PSV Eindhoven",
      "played": 34,
      "points": 76,
      "difference": 45
    },
    {
      "position": 4,
      "name": "Utrecht",
      "played": 34,
      "points": 62,
      "difference": 16
    },
    {
      "position": 5,
      "name": "Vitesse",
      "played": 34,
      "points": 51,
      "difference": 11
    },
    {
      "position": 6,
      "name": "AZ",
      "played": 34,
      "points": 49,
      "difference": 4
    },
    {
      "position": 7,
      "name": "Twente",
      "played": 34,
      "points": 45,
      "difference": "−2"
    },
    {
      "position": 8,
      "name": "Groningen",
      "played": 34,
      "points": 43,
      "difference": 4
    },
    {
      "position": 9,
      "name": "Heerenveen",
      "played": 34,
      "points": 43,
      "difference": 1
    },
    {
      "position": 10,
      "name": "Heracles Almelo",
      "played": 34,
      "points": 43,
      "difference": "−2"
    },
    {
      "position": 11,
      "name": "ADO Den Haag",
      "played": 34,
      "points": 38,
      "difference": "−22"
    },
    {
      "position": 12,
      "name": "Excelsior",
      "played": 34,
      "points": 37,
      "difference": "−17"
    },
    {
      "position": 13,
      "name": "Willem II",
      "played": 34,
      "points": 36,
      "difference": "−15"
    },
    {
      "position": 14,
      "name": "PEC Zwolle",
      "played": 34,
      "points": 35,
      "difference": "−28"
    },
    {
      "position": 15,
      "name": "Sparta Rotterdam",
      "played": 34,
      "points": 34,
      "difference": "−19"
    },
    {
      "position": 16,
      "name": "NEC",
      "played": 34,
      "points": 34,
      "difference": "−27"
    },
    {
      "position": 17,
      "name": "Roda JC Kerkrade",
      "played": 34,
      "points": 33,
      "difference": "−25"
    },
    {
      "position": 18,
      "name": "Go Ahead Eagles",
      "played": 34,
      "points": 23,
      "difference": "−41"
    }
  ]
}
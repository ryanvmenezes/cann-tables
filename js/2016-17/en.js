var en = {
  "competition": "Premier League",
  "code_string": "en",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Premier_League#League_table",
  "teams": 20,
  "cl": [1,2,3,4,6],
  "el": [5,7],
  "relegation": [18,19,20],
  "standings": [
    {
      "position": 1,
      "name": "Chelsea",
      "played": 38,
      "difference": 52,
      "points": 93
    },
    {
      "position": 2,
      "name": "Tottenham Hotspur",
      "played": 38,
      "difference": 60,
      "points": 86
    },
    {
      "position": 3,
      "name": "Manchester City",
      "played": 38,
      "difference": 41,
      "points": 78
    },
    {
      "position": 4,
      "name": "Liverpool",
      "played": 38,
      "difference": 36,
      "points": 76
    },
    {
      "position": 5,
      "name": "Arsenal",
      "played": 38,
      "difference": 33,
      "points": 75
    },
    {
      "position": 6,
      "name": "Manchester United",
      "played": 38,
      "difference": 25,
      "points": 69
    },
    {
      "position": 7,
      "name": "Everton",
      "played": 38,
      "difference": 18,
      "points": 61
    },
    {
      "position": 8,
      "name": "Southampton",
      "played": 38,
      "difference": "−7",
      "points": 46
    },
    {
      "position": 9,
      "name": "Bournemouth",
      "played": 38,
      "difference": "−12",
      "points": 46
    },
    {
      "position": 10,
      "name": "West Bromwich Albion",
      "played": 38,
      "difference": "−8",
      "points": 45
    },
    {
      "position": 11,
      "name": "West Ham United",
      "played": 38,
      "difference": "−17",
      "points": 45
    },
    {
      "position": 12,
      "name": "Leicester City",
      "played": 38,
      "difference": "−15",
      "points": 44
    },
    {
      "position": 13,
      "name": "Stoke City",
      "played": 38,
      "difference": "−15",
      "points": 44
    },
    {
      "position": 14,
      "name": "Crystal Palace",
      "played": 38,
      "difference": "−13",
      "points": 41
    },
    {
      "position": 15,
      "name": "Swansea City",
      "played": 38,
      "difference": "−25",
      "points": 41
    },
    {
      "position": 16,
      "name": "Burnley",
      "played": 38,
      "difference": "−16",
      "points": 40
    },
    {
      "position": 17,
      "name": "Watford",
      "played": 38,
      "difference": "−28",
      "points": 40
    },
    {
      "position": 18,
      "name": "Hull City",
      "played": 38,
      "difference": "−43",
      "points": 34
    },
    {
      "position": 19,
      "name": "Middlesbrough",
      "played": 38,
      "difference": "−26",
      "points": 28
    },
    {
      "position": 20,
      "name": "Sunderland",
      "played": 38,
      "difference": "−40",
      "points": 24
    }
  ]
}
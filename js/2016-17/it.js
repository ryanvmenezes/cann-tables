var it = {
  "competition": "Serie A",
  "code_string": "it",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Serie_A#League_table",
  "teams": 20,
  "cl": [1,2,3],
  "el": [4,5,6],
  "relegation": [18,19,20],
  "standings": [
    {
      "position": 1,
      "name": "Juventus",
      "played": 38,
      "difference": 50,
      "points": 91
    },
    {
      "position": 2,
      "name": "Roma",
      "played": 38,
      "difference": 52,
      "points": 87
    },
    {
      "position": 3,
      "name": "Napoli",
      "played": 38,
      "difference": 55,
      "points": 86
    },
    {
      "position": 4,
      "name": "Atalanta",
      "played": 38,
      "difference": 21,
      "points": 72
    },
    {
      "position": 5,
      "name": "Lazio",
      "played": 38,
      "difference": 23,
      "points": 70
    },
    {
      "position": 6,
      "name": "Milan",
      "played": 38,
      "difference": 12,
      "points": 63
    },
    {
      "position": 7,
      "name": "Internazionale",
      "played": 38,
      "difference": 23,
      "points": 62
    },
    {
      "position": 8,
      "name": "Fiorentina",
      "played": 38,
      "difference": 6,
      "points": 60
    },
    {
      "position": 9,
      "name": "Torino",
      "played": 38,
      "difference": 5,
      "points": 53
    },
    {
      "position": 10,
      "name": "Sampdoria",
      "played": 38,
      "difference": "−6",
      "points": 48
    },
    {
      "position": 11,
      "name": "Cagliari",
      "played": 38,
      "difference": "−21",
      "points": 47
    },
    {
      "position": 12,
      "name": "Sassuolo",
      "played": 38,
      "difference": "−5",
      "points": 46
    },
    {
      "position": 13,
      "name": "Udinese",
      "played": 38,
      "difference": "−9",
      "points": 45
    },
    {
      "position": 14,
      "name": "Chievo",
      "played": 38,
      "difference": "−18",
      "points": 43
    },
    {
      "position": 15,
      "name": "Bologna",
      "played": 38,
      "difference": "−18",
      "points": 41
    },
    {
      "position": 16,
      "name": "Genoa",
      "played": 38,
      "difference": "−26",
      "points": 36
    },
    {
      "position": 17,
      "name": "Crotone",
      "played": 38,
      "difference": "−24",
      "points": 34
    },
    {
      "position": 18,
      "name": "Empoli",
      "played": 38,
      "difference": "−32",
      "points": 32
    },
    {
      "position": 19,
      "name": "Palermo",
      "played": 38,
      "difference": "−44",
      "points": 26
    },
    {
      "position": 20,
      "name": "Pescara",
      "played": 38,
      "difference": "−44",
      "points": 18
    }
  ]
}
var pt = {
  "competition": "Primeira Liga",
  "code_string": "pt",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Primeira_Liga#League_table",
  "teams": 18,
  "cl": [1,2,3],
  "el": [3,4,5],
  "relegation": [17,18],
  "standings": [
    {
      "position": 1,
      "name": "Benfica",
      "played": 34,
      "difference": 54,
      "points": 82
    },
    {
      "position": 2,
      "name": "Porto",
      "played": 34,
      "difference": 52,
      "points": 76
    },
    {
      "position": 3,
      "name": "Sporting CP",
      "played": 34,
      "difference": 32,
      "points": 70
    },
    {
      "position": 4,
      "name": "Vitória de Guimarães",
      "played": 34,
      "difference": 11,
      "points": 62
    },
    {
      "position": 5,
      "name": "Braga",
      "played": 34,
      "difference": 15,
      "points": 54
    },
    {
      "position": 6,
      "name": "Marítimo",
      "played": 34,
      "difference": 2,
      "points": 50
    },
    {
      "position": 7,
      "name": "Rio Ave",
      "played": 34,
      "difference": 2,
      "points": 49
    },
    {
      "position": 8,
      "name": "Feirense",
      "played": 34,
      "difference": "−14",
      "points": 48
    },
    {
      "position": 9,
      "name": "Boavista",
      "played": 34,
      "difference": "−3",
      "points": 43
    },
    {
      "position": 10,
      "name": "Estoril",
      "played": 34,
      "difference": "−6",
      "points": 38
    },
    {
      "position": 11,
      "name": "Chaves",
      "played": 34,
      "difference": "−7",
      "points": 38
    },
    {
      "position": 12,
      "name": "Vitória de Setúbal",
      "played": 34,
      "difference": "−9",
      "points": 38
    },
    {
      "position": 13,
      "name": "Paços de Ferreira",
      "played": 34,
      "difference": "−13",
      "points": 36
    },
    {
      "position": 14,
      "name": "Belenenses",
      "played": 34,
      "difference": "−18",
      "points": 36
    },
    {
      "position": 15,
      "name": "Moreirense",
      "played": 34,
      "difference": "−15",
      "points": 33
    },
    {
      "position": 16,
      "name": "Tondela",
      "played": 34,
      "difference": "−23",
      "points": 32
    },
    {
      "position": 17,
      "name": "Arouca",
      "played": 34,
      "difference": "−24",
      "points": 32
    },
    {
      "position": 18,
      "name": "Nacional",
      "played": 34,
      "difference": "−36",
      "points": 21
    }
  ]
}
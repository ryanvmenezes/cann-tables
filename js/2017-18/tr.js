var tr = {
  "competition": "Süper Lig",
  "code_string": "tr",
  "source": "https://en.wikipedia.org/wiki/2017%E2%80%9318_S%C3%BCper_Lig#League_table",
  "teams": 18,
  "cl": [1,2],
  "el": [3,4,11],
  "relegation": [16,17,18],
  "standings": [
	  {
	    "position": 1,
	    "name": "Galatasaray",
	    "played": 34,
	    "difference": 42,
	    "points": 75
	  },
	  {
	    "position": 2,
	    "name": "Fenerbahçe",
	    "played": 34,
	    "difference": 42,
	    "points": 72
	  },
	  {
	    "position": 3,
	    "name": "Istanbul Basaksehir",
	    "played": 34,
	    "difference": 28,
	    "points": 72
	  },
	  {
	    "position": 4,
	    "name": "Besiktas",
	    "played": 34,
	    "difference": 39,
	    "points": 71
	  },
	  {
	    "position": 5,
	    "name": "Trabzonspor",
	    "played": 34,
	    "difference": 12,
	    "points": 55
	  },
	  {
	    "position": 6,
	    "name": "Göztepe",
	    "played": 34,
	    "difference": -1,
	    "points": 49
	  },
	  {
	    "position": 7,
	    "name": "Sivasspor",
	    "played": 34,
	    "difference": -8,
	    "points": 49
	  },
	  {
	    "position": 8,
	    "name": "Kasimpasa",
	    "played": 34,
	    "difference": -1,
	    "points": 46
	  },
	  {
	    "position": 9,
	    "name": "Kayserispor",
	    "played": 34,
	    "difference": -1,
	    "points": 44
	  },
	  {
	    "position": 10,
	    "name": "Yeni Malatyaspor",
	    "played": 34,
	    "difference": -7,
	    "points": 43
	  },
	  {
	    "position": 11,
	    "name": "Akhisarspor",
	    "played": 34,
	    "difference": -9,
	    "points": 42
	  },
	  {
	    "position": 12,
	    "name": "Alanyaspor",
	    "played": 34,
	    "difference": -4,
	    "points": 40
	  },
	  {
	    "position": 13,
	    "name": "Bursaspor",
	    "played": 34,
	    "difference": -5,
	    "points": 39
	  },
	  {
	    "position": 14,
	    "name": "Antalyaspor",
	    "played": 34,
	    "difference": -9,
	    "points": 38
	  },
	  {
	    "position": 15,
	    "name": "Konyaspor",
	    "played": 34,
	    "difference": -4,
	    "points": 36
	  },
	  {
	    "position": 16,
	    "name": "Osmanlispor",
	    "played": 34,
	    "difference": -1,
	    "points": 33
	  },
	  {
	    "position": 17,
	    "name": "Gençlerbirligi",
	    "played": 34,
	    "difference": -7,
	    "points": 33
	  },
	  {
	    "position": 18,
	    "name": "Kardemir Karabükspor",
	    "played": 34,
	    "difference": -6,
	    "points": 12
	  }
	]
}
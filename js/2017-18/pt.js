var pt = {
  "competition": "Primeira Liga",
  "code_string": "pt",
  "source": "https://en.wikipedia.org/wiki/2017%E2%80%9318_Primeira_Liga#League_table",
  "teams": 18,
  "cl": [1,2],
  "el": [3,4,5],
  "relegation": [17,18],
  "standings": [
    {
      "position": 1,
      "name": "Porto",
      "played": 34,
      "points": 88,
      "difference": 64
    },
    {
      "position": 2,
      "name": "Benfica",
      "played": 34,
      "points": 81,
      "difference": 58
    },
    {
      "position": 3,
      "name": "Sporting CP",
      "played": 34,
      "points": 78,
      "difference": 39
    },
    {
      "position": 4,
      "name": "Braga",
      "played": 34,
      "points": 75,
      "difference": 45
    },
    {
      "position": 5,
      "name": "Rio Ave",
      "played": 34,
      "points": 51,
      "difference": "−2"
    },
    {
      "position": 6,
      "name": "Chaves",
      "played": 34,
      "points": 47,
      "difference": "−8"
    },
    {
      "position": 7,
      "name": "Marítimo",
      "played": 34,
      "points": 47,
      "difference": "−13"
    },
    {
      "position": 8,
      "name": "Boavista",
      "played": 34,
      "points": 45,
      "difference": "−9"
    },
    {
      "position": 9,
      "name": "Vitória de Guimarães",
      "played": 34,
      "points": 43,
      "difference": "−11"
    },
    {
      "position": 10,
      "name": "Portimonense",
      "played": 34,
      "points": 38,
      "difference": "−8"
    },
    {
      "position": 11,
      "name": "Tondela",
      "played": 34,
      "points": 38,
      "difference": "−9"
    },
    {
      "position": 12,
      "name": "Belenenses",
      "played": 34,
      "points": 37,
      "difference": "−13"
    },
    {
      "position": 13,
      "name": "Desportivo das Aves",
      "played": 34,
      "points": 34,
      "difference": "−15"
    },
    {
      "position": 14,
      "name": "Vitória de Setúbal",
      "played": 34,
      "points": 32,
      "difference": "−23"
    },
    {
      "position": 15,
      "name": "Moreirense",
      "played": 34,
      "points": 32,
      "difference": "−21"
    },
    {
      "position": 16,
      "name": "Feirense",
      "played": 34,
      "points": 31,
      "difference": "−16"
    },
    {
      "position": 17,
      "name": "Paços de Ferreira",
      "played": 34,
      "points": 30,
      "difference": "−26"
    },
    {
      "position": 18,
      "name": "Estoril",
      "played": 34,
      "points": 30,
      "difference": "−32"
    }
  ]
}
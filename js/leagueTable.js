// cann table for single league
var teams = league.standings;
var mostPoints = teams[0].points;
var fewestPoints = teams[league.teams - 1].points;
for (var i = mostPoints; i >= fewestPoints; i--) {
  $("<tr id='" + i + "'><td class='points'>" + i + "</td><td class='teams'></td></tr>").appendTo(".league.table tbody");
};
$.each(teams, function() {
  var team = $(this)[0];
  var netGoals = "negative";
  var positiveGoals = "";
  if (team.difference > 0) {
    netGoals = "positive"
    positiveGoals = "+"
  };
  if (team.difference == 0) {
    netGoals = "equal"
  };
  $("#" + team.points + " .points").css("color", "black");
  $("<div class='team-box'><span class='position'>" + team.position + "</span><span class='name'>" + team.name + "</span><span class='played'>" + team.played + "</span><span class='goal-difference " + netGoals + "'>" + positiveGoals + team.difference + "</span></div>").appendTo($("#" + team.points + " .teams"))
});

$(league["cl"]).each(function(){
  var rank = Number(this) - 1;
  $("table .team-box .position").eq(rank).css({"background-color": champions_league, "color": "#fff"});
})

$(league["el"]).each(function(){
  var rank = Number(this) - 1;
  $("table .team-box .position").eq(rank).css({"background-color": europa_league, "color": "#fff"});
})

if ("european_play_offs" in league == true)   {
  $(league["european_play_offs"]).each(function(){
    var rank = Number(this) - 1;
    $("table .team-box .position").eq(rank).css({"background-color": european_playoffs, "color": "#fff"});
  })
  $(".qualifications .legend-entry.el-playoffs").removeClass("hidden");
}

if ("relegation_play_offs" in league == true)   {
  $(league["relegation_play_offs"]).each(function(){
    var rank = Number(this) - 1;
    $("table .team-box .position").eq(rank).css({"background-color": relegation_playoffs, "color": "#fff"});
  })
  $(".qualifications .legend-entry.relegation-playoffs").removeClass("hidden");
}

$(league["relegation"]).each(function(){
  var rank = Number(this) - 1;
  $("table .team-box .position").eq(rank).css({"background-color": relegation, "color": "#fff"});
})

// add source
$("a.source").attr("href", league.source);
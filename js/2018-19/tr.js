var tr = {
  "competition": "Süper Lig",
  "code_string": "tr",
  "source": "https://en.wikipedia.org/wiki/2018%E2%80%9319_S%C3%BCper_Lig#League_table",
  "teams": 18,
  "cl": [1,2],
  "el": [3,4,5],
  "relegation": [16,17,18],
  "standings": [
	  {
	    "position": 1,
	    "name": "Galatasaray",
	    "played": 34,
	    "points": 69,
	    "difference": 36
	  },
	  {
	    "position": 2,
	    "name": "İstanbul Başakşehir",
	    "played": 34,
	    "points": 67,
	    "difference": 27
	  },
	  {
	    "position": 3,
	    "name": "Beşiktaş",
	    "played": 34,
	    "points": 65,
	    "difference": 26
	  },
	  {
	    "position": 4,
	    "name": "Trabzonspor",
	    "played": 34,
	    "points": 63,
	    "difference": 18
	  },
	  {
	    "position": 5,
	    "name": "Yeni Malatyaspor",
	    "played": 34,
	    "points": 47,
	    "difference": 1
	  },
	  {
	    "position": 6,
	    "name": "Fenerbahçe",
	    "played": 34,
	    "points": 46,
	    "difference": 0
	  },
	  {
	    "position": 7,
	    "name": "Antalyaspor",
	    "played": 34,
	    "points": 45,
	    "difference": -16
	  },
	  {
	    "position": 8,
	    "name": "Konyaspor",
	    "played": 34,
	    "points": 44,
	    "difference": 2
	  },
	  {
	    "position": 9,
	    "name": "Alanyaspor",
	    "played": 34,
	    "points": 44,
	    "difference": -6
	  },
	  {
	    "position": 10,
	    "name": "Kayserispor",
	    "played": 34,
	    "points": 41,
	    "difference": -15
	  },
	  {
	    "position": 11,
	    "name": "Çaykur Rizespor",
	    "played": 34,
	    "points": 41,
	    "difference": -2
	  },
	  {
	    "position": 12,
	    "name": "Sivasspor",
	    "played": 34,
	    "points": 41,
	    "difference": -5
	  },
	  {
	    "position": 13,
	    "name": "Ankaragücü",
	    "played": 34,
	    "points": 40,
	    "difference": -15
	  },
	  {
	    "position": 14,
	    "name": "Kasımpaşa",
	    "played": 34,
	    "points": 39,
	    "difference": -9
	  },
	  {
	    "position": 15,
	    "name": "Göztepe",
	    "played": 34,
	    "points": 38,
	    "difference": -5
	  },
	  {
	    "position": 16,
	    "name": "Bursaspor",
	    "played": 34,
	    "points": 37,
	    "difference": -9
	  },
	  {
	    "position": 17,
	    "name": "BB Erzurumspor",
	    "played": 34,
	    "points": 35,
	    "difference": -7
	  },
	  {
	    "position": 18,
	    "name": "Akhisarspor",
	    "played": 34,
	    "points": 27,
	    "difference": -21
	  }
	]
}
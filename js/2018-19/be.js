var be = {
  "competition": "Eerste Klasse A",
  "code_string": "be",
  "source": "https://en.wikipedia.org/wiki/2018–19_Belgian_First_Division_A#League_table",
  "teams": 16,
  "cl": [1,2],
  "el": [3,5,6],
  "relegation": [16],
  "standings": [
  {
    "position": 1,
    "name": "Genk",
    "played": 30,
    "points": 63,
    "difference": 32
  },
  {
    "position": 2,
    "name": "Club Brugge",
    "played": 30,
    "points": 56,
    "difference": 32
  },
  {
    "position": 3,
    "name": "Standard Liège",
    "played": 30,
    "points": 53,
    "difference": 14
  },
  {
    "position": 4,
    "name": "Anderlecht",
    "played": 30,
    "points": 51,
    "difference": 15
  },
  {
    "position": 5,
    "name": "Gent",
    "played": 30,
    "points": 50,
    "difference": 8
  },
  {
    "position": 6,
    "name": "Antwerp",
    "played": 30,
    "points": 49,
    "difference": 5
  },
  {
    "position": 7,
    "name": "Sint-Truiden",
    "played": 30,
    "points": 47,
    "difference": 11
  },
  {
    "position": 8,
    "name": "Kortrijk",
    "played": 30,
    "points": 43,
    "difference": 2
  },
  {
    "position": 9,
    "name": "Charleroi",
    "played": 30,
    "points": 42,
    "difference": 0
  },
  {
    "position": 10,
    "name": "Excel Mouscron",
    "played": 30,
    "points": 40,
    "difference": 0
  },
  {
    "position": 11,
    "name": "Zulte Waregem",
    "played": 30,
    "points": 33,
    "difference": -11
  },
  {
    "position": 12,
    "name": "Eupen",
    "played": 30,
    "points": 32,
    "difference": -23
  },
  {
    "position": 13,
    "name": "Cercle Brugge",
    "played": 30,
    "points": 28,
    "difference": -24
  },
  {
    "position": 14,
    "name": "Oostende",
    "played": 30,
    "points": 27,
    "difference": -23
  },
  {
    "position": 15,
    "name": "Waasland-Beveren",
    "played": 30,
    "points": 27,
    "difference": -13
  },
  {
    "position": 16,
    "name": "Lokeren",
    "played": 30,
    "points": 20,
    "difference": -25
  }
 ]
}
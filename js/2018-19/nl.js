var nl = {
  "competition": "Eredivisie",
  "code_string": "nl",
  "source": "https://en.wikipedia.org/wiki/2018%E2%80%9319_Eredivisie#League_table",
  "last_updated": "2019-05-15T23:25:06Z",
  "teams": 18,
  "cl": [
    1,
    2
  ],
  "el": [
    3,
    4,
    6
  ],
  "relegation": [
	  16,
	  17,
    18
  ],
  "standings": [
	  {
	    "position": 1,
	    "name": "Ajax",
	    "played": 34,
	    "points": 86,
	    "difference": "87"
	  },
	  {
	    "position": 2,
	    "name": "PSV Eindhoven",
	    "played": 34,
	    "points": 83,
	    "difference": "72"
	  },
	  {
	    "position": 3,
	    "name": "Feyenoord",
	    "played": 34,
	    "points": 65,
	    "difference": "34"
	  },
	  {
	    "position": 4,
	    "name": "AZ",
	    "played": 34,
	    "points": 58,
	    "difference": "21"
	  },
	  {
	    "position": 5,
	    "name": "Vitesse",
	    "played": 34,
	    "points": 53,
	    "difference": "19"
	  },
	  {
	    "position": 6,
	    "name": "Utrecht",
	    "played": 34,
	    "points": 53,
	    "difference": "9"
	  },
	  {
	    "position": 7,
	    "name": "Heracles Almelo",
	    "played": 34,
	    "points": 48,
	    "difference": "−7 "
	  },
	  {
	    "position": 8,
	    "name": "Groningen",
	    "played": 34,
	    "points": 45,
	    "difference": "−2 "
	  },
	  {
	    "position": 9,
	    "name": "ADO Den Haag",
	    "played": 34,
	    "points": 45,
	    "difference": "−5 "
	  },
	  {
	    "position": 10,
	    "name": "Willem II",
	    "played": 34,
	    "points": 44,
	    "difference": "−14 "
	  },
	  {
	    "position": 11,
	    "name": "Heerenveen",
	    "played": 34,
	    "points": 41,
	    "difference": "−9 "
	  },
	  {
	    "position": 12,
	    "name": "VVV-Venlo",
	    "played": 34,
	    "points": 41,
	    "difference": "−16 "
	  },
	  {
	    "position": 13,
	    "name": "PEC Zwolle",
	    "played": 34,
	    "points": 39,
	    "difference": "−13 "
	  },
	  {
	    "position": 14,
	    "name": "Emmen",
	    "played": 34,
	    "points": 38,
	    "difference": "−31 "
	  },
	  {
	    "position": 15,
	    "name": "Fortuna Sittard",
	    "played": 34,
	    "points": 34,
	    "difference": "−30 "
	  },
	  {
	    "position": 16,
	    "name": "Excelsior",
	    "played": 34,
	    "points": 33,
	    "difference": "−33 "
	  },
	  {
	    "position": 17,
	    "name": "De Graafschap",
	    "played": 34,
	    "points": 29,
	    "difference": "−37 "
	  },
	  {
	    "position": 18,
	    "name": "NAC Breda",
	    "played": 34,
	    "points": 23,
	    "difference": "−45 "
	  }
	]
}
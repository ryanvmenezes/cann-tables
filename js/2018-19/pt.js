var pt = {
  "competition": "Primeira Liga",
  "code_string": "pt",
  "source": "https://en.wikipedia.org/wiki/2018%E2%80%9319_Primeira_Liga#League_table",
  "last_updated": "2019-05-19T23:15:01Z",
  "teams": 18,
  "cl": [
    1,
    2
  ],
  "el": [
    3,
    4,
    5
  ],
  "relegation": [
    16,
    17,
    18
  ],
  "standings": [
	  {
	    "position": 1,
	    "name": "Benfica",
	    "played": 34,
	    "points": 87,
	    "difference": "72"
	  },
	  {
	    "position": 2,
	    "name": "Porto",
	    "played": 34,
	    "points": 85,
	    "difference": "54"
	  },
	  {
	    "position": 3,
	    "name": "Sporting CP",
	    "played": 34,
	    "points": 74,
	    "difference": "39"
	  },
	  {
	    "position": 4,
	    "name": "Braga",
	    "played": 34,
	    "points": 67,
	    "difference": "19"
	  },
	  {
	    "position": 5,
	    "name": "Vitória de Guimarães",
	    "played": 34,
	    "points": 52,
	    "difference": "12"
	  },
	  {
	    "position": 6,
	    "name": "Moreirense",
	    "played": 34,
	    "points": 52,
	    "difference": "−5 "
	  },
	  {
	    "position": 7,
	    "name": "Rio Ave",
	    "played": 34,
	    "points": 45,
	    "difference": "−2 "
	  },
	  {
	    "position": 8,
	    "name": "Boavista",
	    "played": 34,
	    "points": 44,
	    "difference": "−6 "
	  },
	  {
	    "position": 9,
	    "name": "Belenenses SAD",
	    "played": 34,
	    "points": 43,
	    "difference": "−9 "
	  },
	  {
	    "position": 10,
	    "name": "Santa Clara",
	    "played": 34,
	    "points": 42,
	    "difference": "−2 "
	  },
	  {
	    "position": 11,
	    "name": "Marítimo",
	    "played": 34,
	    "points": 39,
	    "difference": "−18 "
	  },
	  {
	    "position": 12,
	    "name": "Portimonense",
	    "played": 34,
	    "points": 39,
	    "difference": "−15 "
	  },
	  {
	    "position": 13,
	    "name": "Vitória de Setúbal",
	    "played": 34,
	    "points": 36,
	    "difference": "−11 "
	  },
	  {
	    "position": 14,
	    "name": "Desportivo das Aves",
	    "played": 34,
	    "points": 36,
	    "difference": "−14 "
	  },
	  {
	    "position": 15,
	    "name": "Tondela",
	    "played": 34,
	    "points": 35,
	    "difference": "−14 "
	  },
	  {
	    "position": 16,
	    "name": "Chaves",
	    "played": 34,
	    "points": 32,
	    "difference": "−23 "
	  },
	  {
	    "position": 17,
	    "name": "Nacional",
	    "played": 34,
	    "points": 28,
	    "difference": "−40 "
	  },
	  {
	    "position": 18,
	    "name": "Feirense",
	    "played": 34,
	    "points": 20,
	    "difference": "−37 "
	  }
	]
}
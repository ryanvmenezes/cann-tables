var mostPointsAllLeagues = 0;
var fewestPointsAllLeagues = 100;

$(leagues).each(function() {
  var teams = this.standings;
  var number_teams = this["teams"];

  if (teams[0].points > mostPointsAllLeagues) {
    mostPointsAllLeagues = teams[0].points
  };

  if (teams[number_teams - 1].points < fewestPointsAllLeagues) {
    fewestPointsAllLeagues = teams[number_teams - 1].points
  };

  // get last updated
  if (typeof lastUpdated !== 'undefined') {
    var lastUpdatedLeague = new Date(this.last_updated);
    if (lastUpdatedLeague > lastUpdated) {
      lastUpdated = lastUpdatedLeague
    };
  };

  // show last updated
});

if (typeof lastUpdated !== 'undefined') {
  var lastUpdatedString = lastUpdated.toLocaleString('en-GB', {timeZone: 'UTC', timeZoneName: 'short', day: '2-digit', month: 'short', hour: '2-digit', minute: '2-digit'});
  $(".last-update").text(lastUpdatedString)
};

// show grid
function showGrid() {
  $(".columns.tables").toggleClass("is-grid");
}

// total points table
function totalPoints() {
  $(leagues).each(function() {
    var code_string = this["code_string"];
    var number_teams = this["teams"];

    // create table for each competition
    $("<div class='column league'><table class='is-fullwidth total table " + code_string + "'><thead><tr><th class='header-teams tooltip' data-tooltip='" + this.competition + "'><a href='/leagues/" + code_string + "/" + season + ".html'><img src='/img/flags/" + code_string + ".svg' alt='" + code_string + " flag'></a></th></tr></thead><tbody></tbody></table></div>").appendTo(".tables.total");
    var teams = this.standings;
    var mostPoints = teams[0].points;
    var fewestPoints = teams[number_teams - 1].points;

    for (var i = mostPointsAllLeagues; i >= fewestPointsAllLeagues; i--) {
      $("<tr id='" + code_string + "-" + i + "'><td class='teams'><div class='circle-wrap'></div></td></tr>").appendTo("." + code_string + ".total.table tbody");
    };

    // add each team to position based on points
    $.each(teams, function() {
      var team = $(this)[0];
      team.name = team.name.replace(/'/g, '&apos;');
      $("<span class='team-circle tooltip' data-tooltip='" + team.position + ". " + team.name + " (" + team.points + " points, " + team.played + " games)'></span>").appendTo($(".total.table #" + code_string + "-" + team.points + " .teams .circle-wrap"))
    });

    $(this["cl"]).each(function(){
      var rank = Number(this) - 1;
      $(".total.columns ." + code_string + " .team-circle").eq(rank).css("background-color", champions_league);
    })

    $(this["el"]).each(function(){
      var rank = Number(this) - 1;
      $(".total.columns ." + code_string + " .team-circle").eq(rank).css("background-color", europa_league);
    })

    if ("european_play_offs" in this !== false)   {
      $(this["european_play_offs"]).each(function(){
        var rank = Number(this) - 1;
        $(".total.columns ." + code_string + " .team-circle").eq(rank).css("background-color", european_playoffs);
      })
      $(".legend .el-playoffs").removeClass("hidden");
    }

    if ("relegation_play_offs" in this !== false)   {
      $(this["relegation_play_offs"]).each(function(){
        var rank = Number(this) - 1;
        $(".total.columns ." + code_string + " .team-circle").eq(rank).css("background-color", relegation_playoffs);
      })
      $(".legend .relegation-playoffs").removeClass("hidden");
    }

    $(this["relegation"]).each(function(){
      var rank = Number(this) - 1;
      $(".total.columns ." + code_string + " .team-circle").eq(rank).css("background-color", relegation);
    })
  });

  // add most and fewest points
  $(".total.columns .column").first().before("<div class='column points'><table class='is-fullwidth table points'><thead><tr><th class='header-points'><img src='/img/flags/points.svg' alt='empty image'></th></tr></thead><tbody></tbody></table></div>");
  for (var i = mostPointsAllLeagues; i >= fewestPointsAllLeagues; i--) {
    $("<tr id='points-" + i + "'><td class='teams'><div class='points-wrap'></div></td></tr>").appendTo(".points.table tbody");
  };
  $("<span class='listed-points'>" + mostPointsAllLeagues + "</span>").appendTo("#points-" + mostPointsAllLeagues + " .points-wrap");
  $("<span class='listed-points'>" + fewestPointsAllLeagues + "</span>").appendTo("#points-" + fewestPointsAllLeagues + " .points-wrap");
}

totalPoints();

var normalisedPoints = [30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]

// points per game table
function pointsPerGame() {
  $(leagues).each(function() {
    var code_string = this["code_string"];
    var number_teams = this["teams"];

    // create table for each competition
    $("<div class='column league'><table class='is-fullwidth per-game table " + code_string + "'><thead><tr><th class='header-teams tooltip' data-tooltip='" + this.competition + "'><a href='/leagues/" + code_string + "/" + season + ".html'><img src='/img/flags/" + code_string + ".svg' alt='" + code_string + " flag'></a></th></tr></thead><tbody></tbody></table></div>").appendTo(".tables.per-game");
    var teams = this.standings;
    var mostPoints = teams[0].points;
    var fewestPoints = teams[number_teams - 1].points;

    $(normalisedPoints).each(function() {
      $("<tr id='" + code_string + "-" + this + "'><td class='teams'><div class='circle-wrap'></div></td></tr>").appendTo(".per-game ." + code_string + ".per-game.table tbody");
    });

    // add each team to position based on points
    $.each(teams, function() {
      var team = $(this)[0];
      team.name = team.name.replace(/'/g, '&apos;');

      var pointsPerGame = team.points / team.played;
      var roundedPointsPerGame = pointsPerGame.toFixed(1) * 10;

      $("<span class='team-circle tooltip' data-tooltip='" + team.position + ". " + team.name + " (" + pointsPerGame.toFixed(2) + " points per game, " + team.played + " games)'></span>").appendTo($(".per-game.table #" + code_string + "-" + roundedPointsPerGame + " .teams .circle-wrap"))
    });

    $(this["cl"]).each(function(){
      var rank = Number(this) - 1;
      $(".per-game.columns ." + code_string + " .team-circle").eq(rank).css("background-color", champions_league);
    })

    $(this["el"]).each(function(){
      var rank = Number(this) - 1;
      $(".per-game.columns ." + code_string + " .team-circle").eq(rank).css("background-color", europa_league);
    })

    if ("european_play_offs" in this !== false)   {
      $(this["european_play_offs"]).each(function(){
        var rank = Number(this) - 1;
        $("." + code_string + " .team-circle").eq(rank).css("background-color", european_playoffs);
      })
      $(".per-game.columns .legend .el-playoffs").removeClass("hidden");
    }

    if ("relegation_play_offs" in this !== false)   {
      $(this["relegation_play_offs"]).each(function(){
        var rank = Number(this) - 1;
        $("." + code_string + " .team-circle").eq(rank).css("background-color", relegation_playoffs);
      })
      $(".per-game.columns .legend .relegation-playoffs").removeClass("hidden");
    }

    $(this["relegation"]).each(function(){
      var rank = Number(this) - 1;
      $(".per-game.columns ." + code_string + " .team-circle").eq(rank).css("background-color", relegation);
    })


  });
  // add points on left column
  $(".per-game.columns .column").first().before("<div class='column points'><table class='is-fullwidth table points'><thead><tr><th class='header-points'><img src='/img/flags/points.svg' alt='empty image'></th></tr></thead><tbody></tbody></table></div>");
  $(normalisedPoints).each(function() {
    $("<tr id='points-" + this + "'><td class='teams'><div class='points-wrap'></div></td></tr>").appendTo(".per-game.columns .points.table tbody");
  });

  $("<span class='listed-points'>3</span>").appendTo(".per-game.columns #points-30 .points-wrap");
  $("<span class='listed-points'>2</span>").appendTo(".per-game.columns #points-20 .points-wrap");
  $("<span class='listed-points'>1</span>").appendTo(".per-game.columns #points-10 .points-wrap");
  $("<span class='listed-points'>0</span>").appendTo(".per-game.columns #points-0 .points-wrap");
}

pointsPerGame();

// switch table tabs
$(".column.league-tables .tabs a").click(function(){
  $(".columns.tables, .column.league-tables .tabs li").removeClass("is-active");
  $(this).parent().addClass("is-active");

  var tab = $(this).data("tab");
  $(".columns.tables." + tab).addClass("is-active");
});

// show explanation on mobile
function explain() {
  $("p").removeClass("is-hidden-mobile");
  $("#explain").hide();
}

// make table head the same for points
window.onload = function(){
  var tableHeaderHeight = $(".table.nl thead th").height();
  $(".table.points thead th").height(tableHeaderHeight);
};